﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallControl : MonoBehaviour
{

    private Rigidbody2D rigidBody2D;

    public float xInitialForce;
    public float yInitialForce;

    private Vector2 trajectoryOrigin;

    // Start is called before the first frame update
    void Start()
    {
        rigidBody2D = GetComponent<Rigidbody2D>();
        trajectoryOrigin = transform.position;
        RestartGame();
    }

    // Update is called once per frame
    void Update()
    {
    }

    void PushBall()
    {
        float yRandomInitialForce = Random.Range(-yInitialForce, yInitialForce);
        float randomDirection = Random.Range(0, 2);

        //set consistent speed
        //get maximum force possible using initial vector values
        float maxForce = Mathf.Pow(xInitialForce, 2) + Mathf.Pow(yInitialForce, 2);
        //get xUpdatedForce based on yRandomInitialForce
        float xUpdatedForce = Mathf.Sqrt(maxForce - Mathf.Pow(yRandomInitialForce, 2));

        if(randomDirection < 1f)
        {
            rigidBody2D.AddForce(new Vector2(-xUpdatedForce, yRandomInitialForce));
        }
        else
        {
            rigidBody2D.AddForce(new Vector2(xUpdatedForce, yRandomInitialForce));
        }
    }

    void ResetBall()
    {
        transform.position = Vector2.zero;
        rigidBody2D.velocity = Vector2.zero;
    }

    void RestartGame()
    {
        ResetBall();

        Invoke("PushBall", 2);
    }

    public Vector2 TrajectoryOrigin
    {
        get { return trajectoryOrigin; }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        trajectoryOrigin = transform.position;
    }
}
